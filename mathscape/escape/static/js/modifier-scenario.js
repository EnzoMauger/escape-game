// Initialisation des variables pour le canvas
let boxCanvas = document.getElementById('boxCanvas');
let canvas_height = window.innerHeight/1.8;
let canvas_width = window.innerWidth/1.5;
let renderer = PIXI.autoDetectRenderer(canvas_width, canvas_height, {backgroundColor: 'black'});
boxCanvas.parentNode.insertBefore(renderer.view, boxCanvas);

// Initialisation des variables contenant les données de l'escape et de la salleCourante
let listeZones = [], listeIndices = [], listeIndicesInitiale=[], listeZonesInitiale=[];
let nameBackground = "";

function initZones(donnees){
  listeZonesInitiale = donnees;
}
function initIndices(donnees){
  listeIndicesInitiale = donnees;
}
function initBackground(lien){
  nameBackground = lien;
}

// PARTIE MODIFICATIONS
let modeEdition = "nav";
function changerOutil(icone){
  // Changement graphique des boutons
  let ul = document.getElementById('outils');
  let listeIcones = [ul.children[0], ul.children[1], ul.children[2], ul.children[3]]
  listeIcones.forEach(function(icone){
    icone.removeAttribute('selected');
  });
  icone.setAttribute('selected', "");
  modeEdition = icone.attributes['value'].value;

  // Effaçage des events
  listeZones.forEach((sp) => { sp.removeAllListeners(); });
  background.removeAllListeners();

  let origineX, origineY, destinationX, destinationY;
  // réattribution des events
  if(modeEdition == "des"){
    background.on('mousedown', function(e){
      origineX = e.data.global.x;
      origineY = e.data.global.y;
    });

    background.on('mouseup', function(e){
      if(origineX != null){
        destinationX = e.data.global.x;
        destinationY = e.data.global.y;
        tracerRectangle(origineX, origineY, destinationX-origineX, destinationY-origineY);
        origineX = null;
      }
    });
  }else if(modeEdition == "nav"){
    listeZones.forEach((sp) => { sp.on('click', function(sp){
      console.log('affichage des infos');
    });});
  }else if(modeEdition == "sup"){
    listeZones.forEach((sp) => { sp.on('click', supprimerRectangle) });
  }else{
    listeZones.forEach((sp) => {
      sp.on('mousedown', onDragStart);
      sp.on('mouseup', onDragEnd);
      sp.on('mouseupoutside', onDragEnd);
      sp.on('mousemove', onDragMove);
    });
  }
}
function actualiserIndice(input){
  try{
    listeIndices.forEach(function(indice){
      if(indice[0] == input.attributes.ref.value){
        indice[1] = input.value;
        throw BreakException;
      }
    });
  }catch(e){}
}
function actualiserAssociationIndiceEnigme(select){
  let input = select.parentNode.parentNode.children[0];
  try{
    listeIndices.forEach(function(indice){
      if(indice[0] == input.attributes.ref.value){
        indice[2] = select.value;
        throw BreakException;
      }
    });
  }catch(e){}
}

function changerFond(){
  let fichier = document.getElementById('inputImageFond').files[0];
  if(fichier.type.startsWith("image/")){
    let reader = new FileReader();

    reader.addEventListener('load', function(){
      nameBackground = this.result;
      document.getElementById('hiddenImageFond').value = this.result;
      chargerSalle();
    });

    reader.readAsDataURL(fichier);
  }else{
    alert('Veuillez choisir une image');
  }
}
function changerImageEnigme(){
  let fichier = document.getElementById('inputImageEnigme').files[0];
  if(fichier.type.startsWith("image/")){
    let reader = new FileReader();

    reader.addEventListener('load', function(){
      document.getElementById('imageEnigme').src = this.result;
      if(document.getElementById('imageEnigme').style.display == "none"){
        document.getElementById('imageEnigme').style.display = "block";
        document.getElementById('imageEnigme').nextElementSibling.remove();
      }
      document.getElementById('hiddenImageEnigme').value = this.result;
    });

    reader.readAsDataURL(fichier);
  }else{
    alert('Veuillez choisir une image');
  }
}
function changerImageIndice(input){
  let fichier = input.files[0];
  let img = input.parentNode.children[2]

  if(fichier.type.startsWith("image/")){
    let reader = new FileReader();

    reader.addEventListener('load', function(){
      img.src = this.result;
      if(img.style.display == "none"){
        img.style.display = "block";
      }
      for(indice of listeIndices){
        console.log(img);
        if(indice[0] == img.attributes.value.value){
          indice[3] = this.result;
        }
      }
    });

    reader.readAsDataURL(fichier);
  }else{
    alert('Veuillez choisir une image');
  }
}

function onDragStart(event){
    this.dragging = true;
    this.data = event.data;
    this.alpha = 0.5;
}
function onDragEnd(){
    this.dragging = false;
    this.data = null;
    this.alpha = 1;
}
function onDragMove(){
    if(this.dragging){
        const newPosition = this.data.getLocalPosition(this.parent);
        this.x = newPosition.x - (this.width/2);
        this.y = newPosition.y - (this.height/2);
    }
  }

function enregistrerDonnees(){
  if(listeZones != []){ //récupération des zones enregistrées dans la liste
    listeDatasZones = [];
    for(zone of listeZones){
      listeDatasZones.push([(zone.x/canvas_width).toFixed(3), (zone.y/canvas_height).toFixed(3), (zone.width/canvas_height).toFixed(3), (zone.height/canvas_width).toFixed(3)]);
    }
    document.getElementById('inputListeZones').value = listeDatasZones; //attribution en tant que valeur d'un input hidden
    document.getElementById('form_datas').submit(); //envoie du formulaire
  }
}
function sauvegarderIndices(){
  let res = [];
  for(indice of listeIndices){
    if(indice[0].startsWith("nv-") || indice[0].startsWith("del-")){
      indice[1] = indice[1].split(",").join("&1234&");
      indice[3] = indice[3].split(",").join("&1234&");
      res.push(indice[0]+","+indice[1]+","+indice[2]+","+indice[3]);
    }else{
      for(indice2 of listeIndicesInitiale){ //modification de l'indice
        if((indice[0] == indice2[0]) && ((indice[1] != indice2[1]) || (indice[2] != indice2[2]) || (indice[3] != indice2[3]))){
          indice[1] = indice[1].split(",").join("&1234&");
          indice[3] = indice[3].split(",").join("&1234&");
          res.push(indice[0]+","+indice[1]+","+indice[2]+","+indice[3]);
        }
      }
    }
  }

  if(res.length != 0){
    document.getElementById('form_indices').children[0].value = res;
    document.getElementById('form_indices').submit();
  }else{
    closeModal();
  }
}

let nbNvxIndices = 1;
let listeModeleEnigmes = [];
function nouveauIndice(ul){
  if(listeModeleEnigmes.length != 0){
    if(document.getElementById('aucun_indice') != null && document.getElementById('aucun_indice').style.display != "none"){
      document.getElementById('aucun_indice').style.display = "none";
    }

    let li = document.createElement("li");

    let input = document.createElement("input", {type: 'text'});
    input.setAttribute("placeholder", "Écrivez ici !");
    input.setAttribute("onchange", "actualiserIndice(this);");
    input.setAttribute("ref", "nv-"+nbNvxIndices);
    li.appendChild(input);

    let ion = document.createElement("ion-icon");
    ion.setAttribute("size", "large");
    ion.setAttribute("name", "trash");
    ion.setAttribute("onclick", "supprimerIndice(this);");
    li.appendChild(ion);


    let div = document.createElement("div");

    let divInterieur1 = document.createElement("div");

    let divInterieurInterieur = document.createElement("div");
    divInterieurInterieur.setAttribute("couleur", "button");
    divInterieurInterieur.setAttribute("title", "Changer l'image associée à l'indice");
    divInterieurInterieur.setAttribute("onclick", "document.getElementById('inputImageIndice-nv-"+nbNvxIndices+"').click();");
    let ion2 = document.createElement("ion-icon");
    ion2.setAttribute("name", "image");
    ion2.setAttribute("size", "small");
    divInterieurInterieur.appendChild(ion2);
    divInterieur1.appendChild(divInterieurInterieur);

    let input2 = document.createElement("input");
    input2.setAttribute("type", "file");
    input2.setAttribute("onchange", "changerImageIndice(this);");
    input2.setAttribute("id", 'inputImageIndice-nv-'+nbNvxIndices);
    input2.setAttribute("accept", "image/*");
    divInterieur1.appendChild(input2);

    let img = document.createElement("img");
    img.setAttribute("value", "nv-"+nbNvxIndices);
    img.style.display = "none";
    img.setAttribute("title", "Agrandir l'image");
    img.setAttribute("alt", "Image de l'indice");
    img.setAttribute("onclick", "window.open(this.src, '_blank');");
    divInterieur1.appendChild(img);

    div.appendChild(divInterieur1);

    let divInterieur2 = document.createElement("div");
    divInterieur2.innerHTML = "Relié à l'énigme ";
    div.appendChild(divInterieur2);

    let select = document.createElement("select");
    for (elem of listeModeleEnigmes){
      let option = document.createElement("option");
      option.innerHTML = "#"+elem;
      select.appendChild(option);
    }
    div.appendChild(select);

    li.appendChild(div);
    ul.appendChild(li);
    listeIndices.push(["nv-"+nbNvxIndices, "", listeModeleEnigmes[0], ""]);
    nbNvxIndices += 1;
  }else{
    if(ul.children.length == 1){
      let p = ul.children[0];
      p.innerHTML = "Vous devez créer au moins une énigme avant d'ajouter des indices";
      p.style.color = "red";
      ul.appendChild(p);
    }
  }
}
function supprimerRectangle(){
  ecranCourant.removeChild(this);
  listeZones.splice(listeZones.indexOf(this), 1);
}
function supprimerIndice(ion){
  ion.parentNode.parentNode.removeChild(ion.parentNode);
  let input = ion.parentNode.children[0];
  listeIndices.forEach(function(indice){
    if(indice[0] == input.attributes.ref.value){
      if(input.attributes.ref.value.startsWith("nv-")){
        listeIndices.splice(listeIndices.indexOf(indice), 1);
      }else{
        indice[0] = "del-"+indice[0];
      }
    }
  });
}

// Affichage du jeu
let ecranCourant, background;

function renderEcranSalleCourante(){
  requestAnimationFrame(renderEcranSalleCourante);
  renderer.render(ecranCourant);
}
function chargerSalle(){
  ecranCourant = new PIXI.Container();
  if(nameBackground != ""){
    background = new PIXI.Sprite(PIXI.Texture.from(nameBackground));
  }else{
    background = new PIXI.Sprite();
  }
  background.position.set(0, 0);
  background.height = canvas_height;
  background.width = canvas_width;
  background.interactive = true;
  ecranCourant.addChild(background);

  listeZones.splice(0, listeZones.length);
  if(listeZonesInitiale != []){
    for(zone of listeZonesInitiale){
      tracerRectangle(zone[0]*canvas_width, zone[1]*canvas_height, zone[2]*canvas_height, zone[3]*canvas_width);
    }
    changerOutil(document.getElementById('outils').children[0]);
  }

  if(listeIndicesInitiale != []){
    for(indice of listeIndicesInitiale){
      listeIndices.push(indice.slice(0));
    }
  }

  renderEcranSalleCourante();
}

// Ressources
function tracerRectangle(x, y, height, width){
  gt = new PIXI.Graphics();
  gt.lineStyle(0.3,0xD3D3D3, 1);
  gt.drawRoundedRect(0,0,1,1,.1);
  gt.endFill();
  let texture = gt.generateCanvasTexture();

  let sp = new PIXI.Sprite(PIXI.Texture.from(texture));
  sp.x = x;
  sp.y = y;
  sp.width = height;
  sp.height = width;

  sp.interactive = true;
  listeZones.push(sp);

  ecranCourant.addChild(sp);
}

// Dans les modals
const ulListeGroupe = document.getElementById('listeGroupes');
function afficherEnigmeSpeGroupe(idGroupe){
  ulListeGroupe.style.display = "none";
  document.getElementById('GroupeInfosEnigmeSpe-'+idGroupe).style.display = "flex";
}
function cacherEnigmeSpeGroupe(idGroupe){
  ulListeGroupe.style.display = "block";
  document.getElementById('GroupeInfosEnigmeSpe-'+idGroupe).style.display = "none";
}
function choisirGroupePourAssociationEnigmeSpe(id){
  document.getElementById('idGroupeChoisi').value = id;
  document.getElementById('ajouter_groupe').submit();
}
