let liens = document.getElementById('liens');
let icone = liens.parentNode.children[0].children[1];

function initAside(){
  liens.parentNode.style.top = document.getElementById('header').offsetHeight+"px";

  initLiens();
  initMenuEtendu();
}

// ------- INITIALISATIONS --------
function initLiens(){
  let url = window.location.pathname;

  switch(true){
    case url.substring(0, 9) == '/scenario':
    case url.substring(0, 8) == '/escapes':
      liens.children[0].style.color = "#ffffff";
      break;
    case url.substring(0, 8) == '/groupes':
      liens.children[1].style.color = "#ffffff";
      break;
    case url.substring(0, 10) == '/etudiants':
      liens.children[2].style.color = "#ffffff";
      break;
    case url.substring(0, 8) == '/enigmes':
      liens.children[3].style.color = "#ffffff";
      break;
    case url.substring(0, 13) == '/statistiques':
      liens.children[4].style.color = "#ffffff";
      break;
    case url.substring(0, 7) == '/profil':
      liens.children[5].style.color = "#ffffff";
      break;
  }
}

function initMenuEtendu(){
  if(lireCookie('menu_etendu') == "ERREUR"){
    document.cookie="menu_etendu=true";
  }

  icone.parentNode.children[1].style.transition="";
  for(let lien of liens.children){
    lien.children[1].style.transition="";
  }
  if(lireCookie('menu_etendu')){
    etendre();
  }else{
    resserrer();
  }
}

// -------- MODIFICATIONS ---------
function toogleAffichage(){
  icone.parentNode.children[1].style.transition="margin 1s";
  for(let lien of liens.children){
    lien.children[1].style.transition="color 0.2s, margin 1s";
  }
  if(liens.children[0].children[1].style.opacity == "0"){
    etendre();
    document.cookie="menu_etendu=true";
  }else{
    resserrer();
    document.cookie="menu_etendu=false";
  }
}

function etendre(){
  icone.style.opacity = "1";
  icone.style.marginLeft = "0";
  icone.style.zIndex="";
  icone.parentNode.children[0].size="small";
  for(let lien of liens.children){
    lien.children[1].style.opacity = "1";
    lien.children[1].style.marginLeft = "0";
    lien.children[0].size="small";
  }
}

function resserrer(){
  icone.style.marginLeft = "-"+icone.offsetWidth+"px";
  icone.style.opacity="0";
  icone.style.zIndex="-1";
  icone.parentNode.children[0].size="large";
  for(let lien of liens.children){
    lien.children[1].style.opacity="0";
    lien.children[1].style.marginLeft = "-"+lien.children[1].offsetWidth+"px";
    lien.children[0].size="large";
  }
}

// -------- RESSOURCES -----------
function lireCookie(nomCookie){
  let cookies = document.cookie.split(';');
  for( let i=0; i<cookies.length; i++){
    let cook = cookies[i].split('=');
    if(cook[0].replace(' ', '') == nomCookie.replace(' ', '')){
      return JSON.parse(cook[1]); //Pour retourner un boolean et pas un string
    }
  }
  return "ERREUR";
}
