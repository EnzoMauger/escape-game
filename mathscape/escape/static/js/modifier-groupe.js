const ulGroupe = document.getElementById('ulGroupe');
const ulDroite = document.getElementById('ulDroite');
const formulaire = document.getElementById('formulaire_groupe');
let ongletActif = "promo";

function enleveEtuGroupe(id, li){
  for(etu of listePersDansGroupe){
    if(etu[0] == id){
      listePersDansGroupe.splice(listePersDansGroupe.indexOf(etu), 1);
      li.remove();

      if(etu[3] == "aucun groupe"){
        listePersDansAucunGroupe.push(etu);
        if(etu[3] == ongletActif){
          remplacerListeEtu(listePersDansAucunGroupe);
        }
      }else{
        listePersDansPromo.push(etu);
        if(etu[3] == ongletActif){
          remplacerListeEtu(listePersDansPromo);
        }
      }
    }
  }

  majNombreEtudiantsDansListes();
}

function ajouterEtuGroupe(id, lien=null){
  let liste;
  if(ongletActif == "promo"){
    liste = listePersDansPromo;
  }else{
    liste = listePersDansAucunGroupe;
  }

  for(etu of liste){
    if(etu[0] == id){
      listePersDansGroupe.push(etu);
      if(lien){
        lien.remove();
      }else{
        ulDroite.children[liste.indexOf(etu)].remove();
      }
      liste.splice(liste.indexOf(etu), 1);

      if(ongletActif == "promo"){
        let listeIdPers= [];
        for(pers of listePersDansAucunGroupe){
          listeIdPers.push(pers[0]);
        }
        if(listeIdPers.includes(etu[0])){
          listePersDansAucunGroupe.splice(listeIdPers.indexOf(etu[0]), 1);
        }
      }else{
        let listeIdPers= [];
        for(pers of listePersDansPromo){
          listeIdPers.push(pers[0]);
        }
        if(listeIdPers.includes(etu)){
          listePersDansPromo.splice(listeIdPers.indexOf(etu[0]), 1);
        }
      }

      const li = document.createElement("li");
      li.setAttribute("ondblclick", "enleveEtuGroupe("+etu[0]+", this)");
      const div = document.createElement("div");
      const p = document.createElement("p");
      p.innerText = etu[2];
      const h4 = document.createElement("h4");
      h4.innerText = etu[1];
      div.appendChild(h4);
      div.appendChild(p);
      const ion = document.createElement("ion-icon");
      ion.setAttribute("onclick", "enleveEtuGroupe("+etu[0]+", this.parentNode)");
      ion.setAttribute("size", "small");
      ion.setAttribute("name", "arrow-forward-circle-outline");
      li.appendChild(div);
      li.appendChild(ion);
      ulGroupe.appendChild(li);

      majNombreEtudiantsDansListes();

      break;
    }
  }
}

function afficherListeEtu(button, liste){
  button.setAttribute('checked', '');
  if(liste == "aucun groupe"){
    button.nextElementSibling.removeAttribute('checked');
    ongletActif = "aucun groupe";
    remplacerListeEtu(listePersDansAucunGroupe);
  }else{
    button.previousElementSibling.removeAttribute('checked');
    ongletActif = "promo";
    remplacerListeEtu(listePersDansPromo);
  }
}

function remplacerListeEtu(liste){
  while(ulDroite.children.length > 0){
    ulDroite.children[0].remove();
  }

  for(etu of liste){
    const li = document.createElement("li");
    li.setAttribute("ondblclick", "ajouterEtuGroupe("+etu[0]+", this)");
    const div = document.createElement("div");
    const p = document.createElement("p");
    p.innerText = etu[2];
    const h4 = document.createElement("h4");
    h4.innerText = etu[1];
    div.appendChild(h4);
    div.appendChild(p);
    const h3 = document.createElement("h3");
    h3.innerText = etu[1]+" "+etu[2];
    const ion = document.createElement("ion-icon");
    ion.setAttribute("onclick", "ajouterEtuGroupe("+etu[0]+", this.parentNode)");
    ion.setAttribute("size", "small");
    ion.setAttribute("name", "arrow-back-circle-outline");
    li.appendChild(ion);
    li.appendChild(div);
    ulDroite.appendChild(li);
  }
}

function envoyerFormulaire(){
  formulaire.children[0].value = listePersDansGroupe;
  formulaire.submit();
}

function genererGroupe(button){
  let liste;
  if(ongletActif == "promo"){
    liste = listePersDansPromo;
  }else{
    liste = listePersDansAucunGroupe;
  }

  let input = button.parentNode.nextElementSibling;
  if(input.value > liste.length){
    if(ongletActif == "promo")
      alert("Veuillez choisir une valeur inférieur ou égale au nombre d'étudiants dans la promotion ("+liste.length+")");
    else{
      alert("Veuillez choisir une valeur inférieur ou égale au nombre d'étudiants qui ne sont pas encore dans un groupe("+liste.length+")");
    }
  }else{
    if(input.value == liste.length){
      while(liste.length > 0){
        ajouterEtuGroupe(liste[0][0]);
      }
    }else{
      for(let i=0; i<input.value; i++){
        randomEtu = liste[Math.floor(Math.random()*liste.length)];
        ajouterEtuGroupe(randomEtu[0]);
      }
    }
    closeModal();
  }
}

function majNombreEtudiantsDansListes(){
  ulDroite.previousElementSibling.children[0].innerText = ulDroite.previousElementSibling.children[0].innerText.substr(0, 17);
  ulDroite.previousElementSibling.children[1].innerText = ulDroite.previousElementSibling.children[1].innerText.substr(0, 17);
  ulDroite.previousElementSibling.children[0].innerText += " ("+listePersDansAucunGroupe.length+")";
  ulDroite.previousElementSibling.children[1].innerText += " ("+listePersDansPromo.length+")";
}

function viderGroupe(){
  //pour chaque etudiant dans la liste groupe
  for(etu of listePersDansGroupe){
    //on retire le li de l'etudiant de la liste
    ulGroupe.children[0].remove();

    if(etu[3] == "aucun groupe"){
      listePersDansAucunGroupe.push(etu);
    }else{
      listePersDansPromo.push(etu);
    }
  }
  if(ongletActif == "promo"){
    remplacerListeEtu(listePersDansPromo);
  }else{
    remplacerListeEtu(listePersDansAucunGroupe);
  }
  listePersDansGroupe.splice(0, listePersDansGroupe.length);
  majNombreEtudiantsDansListes();
}

//PAS UTILISER POUR L'INSTANT
function rechercheBarMGroupe(){
  //recuperer la valeur dans la rechercheBar
  var input = document.getElementById("barreRechercheMGroupe").value;
  //creer la futur liste
  var res = [];
  //pour chaque membre de cette liste
  for(etu of listePersDansGroupe){
    //on regarde si le prenom est egal a la valeur de la barre de recherche, si ce n'est pas le cas on regarde si le nom est egal
    if (etu[1] != input){
      if (etu[2] != input){
        //on ajoute l'etudiant dans la liste
        res.push(etu);
      }
    }
  }
  //on affiche les elems selectionne
}


function rechercheBarEtuInscrit(){
  //recuperer la valeur dans la rechercheBar
  var input = document.getElementById("barreRechercheEtuInscrit").value.toLowerCase();
  console.log(input);
  if (input != ""){
    //recuperer la liste que l'on veut traiter
    if(ongletActif == "promo"){
      var liste = listePersDansPromo;
    }
    else{
      var liste = listePersDansAucunGroupe;
    }
    res = [];
    for (etu of liste){
      if (etu[1].toLowerCase().indexOf(input) > -1){
        res.push(etu);
      }
      if (etu[2].toLowerCase().indexOf(input) > -1){
        res.push(etu);
      }
    }
    for (etu of liste){
      if (etu.indexOf(res) == false){
        res.push(etu);
      }
    }
    //on affiche les elems selectionne
    remplacerListeEtu(res);
  }
}
