let modal;

function afficherModal(idModal){
  modal = document.getElementById(idModal);
  modal.style.display = "flex";
}

function closeModal(){
  modal.style.display = "none";
}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
